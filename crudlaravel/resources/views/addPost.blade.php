<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>all title</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" 
    integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
<section class='padding-top:60px;'>
    <div class="container">
      <div class="row">
        <div class="col-md-8">
        <div class="card" >
          <div class="card-header">
            Add Post
          </div>
 
 <div class="card-body">
 @if(Session::has('Post-create'))
 <div class='alert alert-success' role='alert'>
   {{Session::get('Post-create')}}

 </div>
 @endif
 
   <form method='Post' action="{{route('create.post')}}">
      @csrf 
      <div class="form-group">
          <label forhtml="title"> post title</label>
      <input type="text" name='title'class='form-control' placeholder='entrer post title' >

      </div>
      <div class="form-group">
          <label forhtml="body"> post description</label>
          <textarea name="body" class='form-control' cols="30" rows="10"></textarea>
     

      </div>
      <button type="submit" class='btn btn-success'>Add Post</button>
     
   </form>
 </div>
</div>


        </div>
      </div>
    </div>
  </section>

<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" 
integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" 
integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    
</body>
</html>