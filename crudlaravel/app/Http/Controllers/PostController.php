<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;

class PostController extends Controller
{
   
    public function AddPost(){
        return view('addPost');
      
     
    }
    public function createPost( Request $request){
        $post=new Post();
        $post->title=$request->title;
        $post->body=$request->body;
        $post->save();

        return back()->with('Post-create','Post has benn create successesful');
    }
    public function getPost(){
        $posts=Post::orderBy('id','DESC')->get();
        return view('post',compact('posts'));
    }
   public function getPostById($id){
       $post=Post::where('id',$id)->first();
       return view('single-post',compact('post'));

   }
    
   public function deletePost($id){
    Post::where('id',$id)->delete();
    return back()->with('Post-delete','Post has benn delete successesful');
   }
   public function editPost($id){
       $post=Post::find($id);
       return view('edit-post',compact('post'));
   }
   public function updatePost(Request $request){
       $post=Post::find($request->id);
       $post->title=$request->title;
       $post->body=$request->body;
       $post->save();
       return back()->with('Post-update','Post has benn update successesful');
   }
}
